/* 
    GULP DEPENDENCIES
    To extend functionality, require more goodness here.
*/

'use strict';

const gulp = require('gulp'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    jshint = require('gulp-jshint'),
    sourcemaps = require('gulp-sourcemaps'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant');
/* 
    OUR LOCAL CONFIGURATION
    Change the Variables below for your local rig
*/    
let themeDir = './httpdocs/wp-content/themes/your-theme-name/',
    // All of our custom js
    appScripts = [
        themeDir + 'js/!(init).js', 
        themeDir + 'js/init.js'
    ],
    // All of the js libraries we want
    // right now, you can just do bower install <package_name> and then add the path below
    vendorScripts = [
        themeDir + 'bower_components/example/example.js',
    ];
/*
    OUR TASKS
*/
gulp.task('sass', () => {
    gulp.src([themeDir + 'css/**/*.+(css|scss)'])
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(themeDir))
});

gulp.task('js', () => {
    // Main app scripts
    gulp.src(appScripts)
        .pipe(jshint())
        .pipe(jshint.reporter(''))
        .pipe(sourcemaps.init({ base: themeDir }))
        .pipe(concat('app.js'))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(themeDir))
    // Vendor Scripts 
    gulp.src(vendorScripts)
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest(themeDir))
});


// Quick Utility to minify images should you want it
gulp.task('img', () => {
    return gulp.src(themeDir + 'images/*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest(themeDir + 'images'));
});

// The Default task we use to kick it all off
gulp.task('default', ['sass', 'js'], () => {
    gulp.watch(themeDir + '**/*.+(css|scss)', ['sass']);
    gulp.watch(themeDir + 'js/**/*.js', ['js']);
})