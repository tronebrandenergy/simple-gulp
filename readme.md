# Simple Gulp Rig

The purpose of this rig is to give you something uncomplicated but opinionated enought to be helpful.
    * I've elminated multiple files and stuck with one gulp.js file 
    * I build this on the opinion that we should have fewer css and js files in total. 

* SASS task will produce regular css files with accompanying source maps  
    ** style.css
    
* JS task will concatenate and produce 2 files.
        *** app.js
        *** vendor.js
    ** Your best bet is to bower install any dependencies and link them in through the config section at the top of the gulp file. 
    
## GETTING STARTED
``` 
    npm install 
```
and then you can type
```
    gulp
```


    